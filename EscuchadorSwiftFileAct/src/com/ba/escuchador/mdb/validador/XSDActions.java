package com.ba.escuchador.mdb.validador;


import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.Optional;
import javax.xml.validation.Schema;
import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.xml.sax.SAXException;

import com.ba.escuchador.mdb.propiedades.Constantes;
import com.ba.escuchador.mdb.propiedades.Propiedades;
import com.ba.escuchador.mdb.util.SalidaLogger;

public class XSDActions {

	
    public static boolean formatCheck(String filename) {
        Optional<String> extension = Optional.ofNullable(filename)
        .filter(f -> f.contains("."))
        .map(f -> f.substring(filename.toLowerCase().lastIndexOf(".xml") + 1));
        return extension.get().equals("xml");
    }

    public static boolean validate(String msg) {
        
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        
        try {
        	File xsd =  new File(Propiedades.obtenerPropiedadPorNombre(Constantes.PATH_XSD).trim());
            Schema schema = schemaFactory.newSchema(xsd);
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(new StringReader(msg)));
            return true;
        } catch (SAXException e) {
        	SalidaLogger.logLnX("El mensaje enviado no se encuentra bien formado bajo los estandares de pain.001.001.002");
        	SalidaLogger.logErrorStackTrace(e);
        }
        catch(IOException e){
        	SalidaLogger.logLnX("No se encontro el archivo XSD para la confrontación de información");
        	SalidaLogger.logErrorStackTrace(e);
        }
        return false;
    }
}

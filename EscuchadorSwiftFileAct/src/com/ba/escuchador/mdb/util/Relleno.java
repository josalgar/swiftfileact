package com.ba.escuchador.mdb.util;

import java.text.SimpleDateFormat;
import java.util.Date;



public class Relleno {
	public static final String FORMATTER_DATE_TIME_SERVICIO_IBS = "yyyyMMddHHmmss";
	
	public static String pad(String str, int len, boolean fillRight) {
		if (str.length() < len) {
			StringBuffer sb = new StringBuffer();
			while (sb.length() < len - str.length()) {
				sb.append("         ");
			}
			sb.setLength(len - str.length());
			str = (fillRight) ? (str + sb) : (sb + str);
		}
		// assert(str.length() == len);

		return str.substring(0, len);
	}

	public static String pad(String str, int len) {
		return pad(str, len, true);
	}
	
	public static String numberPadding(long number, int length, char padding) {
		String str = Long.toString(number);

		// assert str.length() <= length;
		StringBuffer sb = new StringBuffer(length);
		for (int i = length - str.length(); i > 0; i--) {
			sb.append(padding);
		}
		sb.append(number);
		
		if (sb.toString().length() > length) {
			return sb.toString().substring(0,length);
		} else {
			return sb.toString();
		}
	}
	


public static String dateTimeServicioIBS(Date date)
	{
		try {
			return new SimpleDateFormat(FORMATTER_DATE_TIME_SERVICIO_IBS).format(date);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}	    
	}

public static String idPadding(String id, int length, char padding){

	// assert str.length() <= length;
	StringBuffer sb = new StringBuffer(length);
	for (int i = length - id.length(); i > 0; i--) {
		sb.append(padding);
	}
	sb.append(id);
	
	if (sb.toString().length() > length) {
		return sb.toString().substring(0,length);
	} else {
		return sb.toString();
	}
	}
}

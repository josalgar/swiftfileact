package com.ba.escuchador.mdb.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Calendar;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.ba.escuchador.mdb.propiedades.Constantes;

public class SalidaLogger {
	private static boolean logActivo = true;
	private static final Logger _loggerInfo = Constantes.infoLogger;
	private static final Logger _loggerError = Constantes.errorLogger;
	
	
	// Errores proveniente de Constante
	public static void logErrorStackTrace(Exception e) {
		StringWriter sw = null;
		PrintWriter pw = null;
		try {
			sw = new StringWriter();
			pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			_loggerError.error(sw.toString());
		} catch (Exception ex) {
			_loggerError.error("Error al imprimir el stacktrace como string por favor verificar en SystemOut.log el detalle completo del error");
			ex.printStackTrace();
		} finally {
			try {
				if (sw != null)
					sw.close();
				if (pw != null)
					pw.close();
			} catch (Exception e2) {
				_loggerError.error("Error al cerrar PrintWriter y StringWriter en SystemOut.log el detalle completo del error");
				e2.printStackTrace();
			}
		}
	}

	public static void logLnX(String datos) {
		if (logActivo)
			logLn(datos);
	}

	public static void logLn(String datos) {
		_loggerInfo.info("");
		_loggerInfo.info(datos);
	}

	public static void logErr(String datos) {
		Calendar c = Calendar.getInstance();
		StringBuilder sb = new StringBuilder();
		sb.append(c.get(Calendar.DAY_OF_MONTH)).append("-").append((c.get(Calendar.MONTH) + 1)).append("-")
				.append(c.get(Calendar.YEAR)).append(":").append(c.get(Calendar.HOUR_OF_DAY)).append(":")
				.append(c.get(Calendar.MINUTE)).append(":").append(Calendar.SECOND).append("<<").append(datos)
				.append("\n\r");
		System.err.println(sb.toString());
	}

	/**
	 * Inicia el logger de log4j para escribir en el archivo
	 */
	public static void initLoggers() {
		try {
			File file = null;

			file = new File(Constantes.PATH_LOG4J);
			System.out.println("initLoggers()=" + file.getCanonicalPath() + ",exists=" + file.exists());
			PropertyConfigurator.configure(file.getCanonicalPath());
			//Logger.getLogger(Constantes.LOGGER_NAME).setAdditivity(false);
		} catch (IOException ioe) {
			System.out.println("No existe archivo de propiedades");
			ioe.printStackTrace();
		}
	}
}

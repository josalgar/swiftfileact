package com.ba.escuchador.mdb.util;

public class ControladorErrores {
	
	public static void verificarTrama(String trama) throws Exception{
		String program = trama.substring(12, 17);

		if (program.equals("ERROR")) {
			int index = trama.indexOf(':');
			String error = trama.substring(index+1, index+4);
			switch (error) {
			case "500":
				SalidaLogger.logLnX("El n�mero de transacciones no concide con la etiqueta NbOfTxs");
			break;
			case "404":
				SalidaLogger.logLnX("No existe dicho formato de trama");
			break;
			default:
				SalidaLogger.logLnX("Error no registrado contacte al equipo desarrollador");
				break;
			}
			throw new Exception("Error en la lectura del XSLT, confirme que contenga los datos correctos");
		}
	}
}

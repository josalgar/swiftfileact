package com.ba.escuchador.mdb.util;


import com.ba.escuchador.mdb.pruebas.PruebasPLSAL;
import com.ba.escuchador.mdb.pruebas.PruebasPPACH;
import com.ba.escuchador.mdb.pruebas.PruebasPPROV;
import com.ba.escuchador.mdb.pruebas.PruebasUtil;
import com.ba.escuchador.mdb.pruebas.PruebasXSD;
import com.ba.escuchador.mdb.pruebas.PruebasXSL;

public class ControladorPruebas {
	private static boolean fallos = false;
	
	public static void comenzar() {
		SalidaLogger.logLnX("--> Inician pruebas unitarias ");
		
		int totalFails = PruebasXSD.pruebas()+
				PruebasXSL.pruebas()+
				PruebasPPACH.pruebas()+ 
				PruebasPPROV.pruebas() + 
				PruebasPLSAL.pruebas() + 
				PruebasUtil.pruebas();
	    if(totalFails>0){
	    	fallos =  true;
	    	SalidaLogger.logLnX("--> Fallaron un total de "+totalFails+" pruebas unitarias");
	    	return;
	    }
	    fallos = false;
	    SalidaLogger.logLnX("--> Todos los casos de prueba pasaron exitosamente ");
	}
	public static boolean isFallos() {
		return fallos;
	}
	public static void setFallos(boolean fallos) {
		ControladorPruebas.fallos = fallos;
	} 
	
	
}

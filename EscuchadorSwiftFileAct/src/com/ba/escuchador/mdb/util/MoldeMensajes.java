package com.ba.escuchador.mdb.util;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

public class MoldeMensajes {
	public static String castString(Message msg) throws JMSException{
		if(msg instanceof TextMessage){
			SalidaLogger.logLnX("--- Mensaje tipo TextMessage recibido");
			TextMessage mensaje = (TextMessage) msg;
			return mensaje.getText();
		}
		else if(msg instanceof BytesMessage){
			SalidaLogger.logLnX("--- Mensaje tipo BytesMessage recibido");					
			BytesMessage byteMessage = (BytesMessage) msg;
			byte[] byteData = null;
			byteData = new byte[(int) byteMessage.getBodyLength()];
			byteMessage.readBytes(byteData);
			byteMessage.reset();
			return new String(byteData);
		}
		return null;
	}
}

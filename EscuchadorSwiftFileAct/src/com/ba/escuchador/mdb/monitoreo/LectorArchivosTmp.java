package com.ba.escuchador.mdb.monitoreo;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.ba.escuchador.mdb.operatividad.OperacionCentral;
import com.ba.escuchador.mdb.propiedades.Constantes;
import com.ba.escuchador.mdb.propiedades.Propiedades;
import com.ba.escuchador.mdb.transformador.MRM;
import com.ba.escuchador.mdb.util.SalidaLogger;

public class LectorArchivosTmp implements Runnable {

	boolean exit = false;
	
	private String nombre; 
    public Thread t; 
	
	public LectorArchivosTmp(String nombre) {
		 this.nombre = nombre; 
		 t = new Thread(this, this.nombre); 
		 System.out.println("New thread: " + t); 
		 exit = false; 
		 t.start(); 
	}
	
	public void run() {

		//while(!exit){
			OperacionCentral centro = new OperacionCentral();

			try (Stream<Path> walk = Files.walk(Paths.get(Propiedades.mapa.get(Constantes.RUTA_TMP)))) {
				List<String> archivos = walk.filter(Files::isRegularFile).map(x -> x.toString())
						.collect(Collectors.toList());
				StringBuilder cuerpo = new StringBuilder();
	
				for (String tmp : archivos) {
					cuerpo.setLength(0);
					for (String linea : Files.readAllLines(Paths.get(tmp))) {
						cuerpo.append(linea+"\n");
					}
					MRM mrm = new MRM(cuerpo.toString());
					centro.procesarTrama(mrm);
				}
	
			} catch (IOException e) {
				SalidaLogger.logErrorStackTrace(e);
			}
		//}

	}
	
	public void stop() {
		exit = false;
	}

}

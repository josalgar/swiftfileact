package com.ba.escuchador.mdb.ejb;


import javax.annotation.PostConstruct;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;

import com.ba.escuchador.mdb.monitoreo.GestorHilos;
import com.ba.escuchador.mdb.operatividad.ConfiguracionSFTP;
import com.ba.escuchador.mdb.operatividad.OperacionCentral;
import com.ba.escuchador.mdb.propiedades.Propiedades;
import com.ba.escuchador.mdb.propiedades.PropiedadesMQ;
import com.ba.escuchador.mdb.util.ControladorPruebas;
import com.ba.escuchador.mdb.util.MoldeMensajes;
import com.ba.escuchador.mdb.util.SalidaLogger;

//import com.ba.archivos.monitoreo.operatividad.sftp.MonitorArchivosSftp;
//import com.ba.archivos.monitoreo.propiedades.Constantes;
//import com.ba.archivos.monitoreo.propiedades.Propiedades;
//import com.ba.archivos.monitoreo.sftp.EjecucionMonitorSftp;


@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "jms/EscuchadorSwiftFileAct"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue") }, mappedName = "jms/EscuchadorSwiftFileAct")

public class EscuchadorBeanPrincipal implements MessageListener {
	private static OperacionCentral centro;
	private static boolean sinFallos = true;
	
	@PostConstruct
	public void init() {
		SalidaLogger.initLoggers();
		SalidaLogger.logLnX("Inicia ejbCreate()");
		Propiedades.iniciarPropiedades();
		PropiedadesMQ.inicializarPropieadesMQ();
		ConfiguracionSFTP.establecerPropiedades();
		
		ControladorPruebas.comenzar();
		
		GestorHilos.correrLectorArchivosTmp();
		
		sinFallos = ControladorPruebas.isFallos();
		centro= new OperacionCentral();
		SalidaLogger.logLnX("Finaliza ejbCreate()");
	}

	/**
	 * ebancaEmpresa MultiplesAbonoPlanilla->CuerpoMensajenotificacion
	 * 
	 * @see MessageListener#onMessage(Message)
	 */
	public void onMessage(Message msg) {
		
		SalidaLogger.logLnX("-->>Ingresando metodo onMessage..........................................");
		

		String stringMessage = null;
//		if(sinFallos){
//			SalidaLogger.logLnX("--> MENSAJE IGNORADO DENTRO DEL PROCESO. LAS PRUEBAS NO PASARON CON EXITO <--");
//		}
		if (centro == null) {
			SalidaLogger.logLnX("--> OCURRIO UN ERROR AL INICIAR EL ESCUCHADOR VERIFICAR EL HISTORIAL DEL LOG <--");
		} 
		else {
			try {
				SalidaLogger.logLnX("-->>toString"+msg.toString());				
				SalidaLogger.logLnX("------------------------------------------------------------------");
					
				stringMessage = MoldeMensajes.castString(msg);
				
				SalidaLogger.logLnX(">>SWIFT<< mensaje= " + stringMessage);

				if (stringMessage != null) {
					long current = System.currentTimeMillis();
					SalidaLogger.logLnX("Iniciando ejecucion de procesamiento de archivo >>>  ");
					centro.procesarMensaje(stringMessage);
					SalidaLogger.logLnX("Fin de ejecucion de procesamiento de archivo. Tiempo total en milisegundos transcurido en el procesamiento: "+(System.currentTimeMillis()-current)+" milisegundos >>> ");
				} 
				else {
					SalidaLogger.logErr("Error en la transimisición de mensaje. Type NULL");
				}
			} catch (Exception e) {
				e.printStackTrace();
				SalidaLogger.logErr("Error al procesar mensaje: " + e.getMessage());
				SalidaLogger.logErrorStackTrace(e);
			}

		}

	}
}

package com.ba.escuchador.mdb.mq;

import com.ba.escuchador.mdb.propiedades.PropiedadesMQ;
import com.ba.escuchador.mdb.util.SalidaLogger;
import com.ibm.mq.MQC;
import com.ibm.mq.MQEnvironment;
import com.ibm.mq.MQException;
import com.ibm.mq.MQMessage;
import com.ibm.mq.MQPutMessageOptions;
import com.ibm.mq.MQQueue;
import com.ibm.mq.MQQueueManager;

public class Notificacion {

	public static boolean enviarRespuestaMQ(String cuerpoMensaje, byte[] correlation) {
		SalidaLogger.logLnX(" >> EscuchadorBean, enviarRespuestaMQ(): ingresa a enviar respuesta");
		
		if(!PropiedadesMQ.mapa.isEmpty() & PropiedadesMQ.mapa.size()==5){
			SalidaLogger.logLnX(" >> EscuchadorBean, enviarRespuestaMQ(): se intenta enviar respuesta por MQ ");
			MQQueueManager qMgr = null;
			
			MQQueue queue = null;
			MQQueue respQueue = null;
			try{
				
				MQEnvironment.hostname = PropiedadesMQ.obtenerPropiedadPorPosicion(0);
				
				MQEnvironment.port = Integer.parseInt(PropiedadesMQ.obtenerPropiedadPorPosicion(2));
				
				MQEnvironment.channel = PropiedadesMQ.obtenerPropiedadPorPosicion(3);
				
				//MQEnvironment.properties.put(MQC.TRANSPORT_PROPERTY,MQC.TRANSPORT_MQSERIES);
				
				qMgr = new MQQueueManager(PropiedadesMQ.obtenerPropiedadPorPosicion(1));
				
				int openOptions = MQC.MQOO_FAIL_IF_QUIESCING | MQC.MQOO_OUTPUT;
				
				queue =qMgr.accessQueue(PropiedadesMQ.obtenerPropiedadPorPosicion(4),openOptions,null,null,null);
				

				MQMessage outMsg = new MQMessage();
				outMsg.format = MQC.MQFMT_STRING;
				outMsg.messageFlags = MQC.MQMT_REQUEST;
				
				if(correlation!=null){
					outMsg.correlationId = correlation;
				}	
//				outMsg.replyToQueueManagerName = element.getQMResponse();
//				outMsg.replyToQueueName        = element.getQueueResponse();
				
				outMsg.writeString(cuerpoMensaje);
				
				MQPutMessageOptions pmo = new MQPutMessageOptions();
				pmo.options = pmo.options + MQC.MQPMO_NEW_MSG_ID ;
				pmo.options = pmo.options + MQC.MQPMO_SYNCPOINT ;
				
								
			
				//Expiración

				queue.put(outMsg,pmo);
				
				qMgr.commit();
				
				//Operacion satisfactoria
				SalidaLogger.logLnX(" >> EscuchadorBean, enviarRespuestaMQ(): se ha enviado respuesta por MQ ");
				
								
			}
			catch (MQException ex){
				SalidaLogger.logErr("[ERROR-MQ]An MQ Error Occurred:Completion Code is :\t" + ex.completionCode + "\n \n The Reason Code is :\t" + ex.reasonCode );
				SalidaLogger.logErrorStackTrace(ex);
				return false;
			}
			catch(Exception e){
				
				SalidaLogger.logErr("[ERROR-MQ]:" + e);
				SalidaLogger.logErrorStackTrace(e);
				return false;
			}
			finally{
				try{if(queue!=null)queue.close();}catch(Exception e){e.printStackTrace();}
				try{if(respQueue!=null)respQueue.close();}catch(Exception e){e.printStackTrace();}
				try{if(qMgr!=null)qMgr.disconnect();}catch(Exception e){e.printStackTrace();}
				try{if(qMgr!=null)qMgr.close();}catch(Exception e){e.printStackTrace();}
			}
			return true;
			
		}
		return false;

	}
}

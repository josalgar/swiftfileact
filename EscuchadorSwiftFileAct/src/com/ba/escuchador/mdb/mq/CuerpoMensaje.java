package com.ba.escuchador.mdb.mq;

import java.util.Date;

import com.ba.escuchador.mdb.propiedades.Constantes;
import com.ba.escuchador.mdb.propiedades.Propiedades;
import com.ba.escuchador.mdb.transformador.GeneradorNombre;
import com.ba.escuchador.mdb.transformador.MRM;
import com.ba.escuchador.mdb.util.Relleno;


public class CuerpoMensaje {
	public static String crearMensaje(MRM mrm){
        
		Date currentDate = new Date();
		//Propiedades.obtenerPropiedadPorNombre(Constantes.CLAVE_ESCRIBE)
		
		String id	   = Propiedades.obtenerPropiedadPorNombre(Constantes.ID_SERVICIO);
		String idTransaccion = Relleno.idPadding(mrm.getMsgId(), 15, '0');
		String fecha    =  Relleno.dateTimeServicioIBS(currentDate);
		String userSrv  =  Relleno.pad("ebanca",15);
		String agencia  =  Relleno.pad("037", 10, false); 
		String sistema  =  "PLANILLFTP";
		String programa =  Relleno.pad(mrm.getTipo(), 10);
		String relleno1  =  Relleno.numberPadding(0, 256, '0' );
		String cliente  =  "GENMSWIFT";
		String relleno2  =  Relleno.numberPadding(0, 54, '0' );
		String nombreArchivoPeticion = mrm.getNombre();
		String ip      = Relleno.pad(Propiedades.obtenerPropiedadPorNombre(Constantes.SERVIDOR_ESCRIBE), 15, true);
		String usuario = Relleno.pad(Propiedades.obtenerPropiedadPorNombre(Constantes.USUARIO_CORE), 12, true);
		String clave = Propiedades.obtenerPropiedadPorNombre(Constantes.CLAVE_CORE);
		String password = "";
		if(clave.equals("0")){
			 password = Relleno.pad("", 12, true);
		}
		else{
			password = Relleno.pad(clave, 12, true);
		}
		String nombreArchivoRespuesta = Relleno.pad("R"+GeneradorNombre.formularNombreMRM(), 10, true);
		String rutaPet = Relleno.pad(Propiedades.obtenerPropiedadPorNombre(Constantes.LECTURA_CORE), 60, true);
        String rutaRes = Relleno.pad(Propiedades.obtenerPropiedadPorNombre(Constantes.RESPUESTA_CORE), 60, true);
        String nombreColaRespuestaBROKER = Relleno.pad(Propiedades.obtenerPropiedadPorNombre(Constantes.COLA_RESPUESTA), 50, true);
        String valorTipoPlanilla = Relleno.pad( mrm.getTipo(), 5, true);

        StringBuilder tramaMensaje = new StringBuilder(""); 
        tramaMensaje.setLength(0);
        		tramaMensaje.append(id).append(idTransaccion).append(fecha).append(userSrv)
        		.append(agencia).append(sistema).append(programa).append(relleno1).append(cliente)
        		.append(relleno2).append(nombreArchivoPeticion).append(ip).append(usuario)
        		.append(password).append(nombreArchivoRespuesta).append(rutaPet).append(rutaRes)
        		.append(nombreColaRespuestaBROKER).append(valorTipoPlanilla);
//        String tramaMensaje = id + idTransaccion + fecha + userSrv + agencia +sistema + programa + relleno1 + cliente+ re
        return tramaMensaje.toString();
	}
}

package com.ba.escuchador.mdb.pruebas;

import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ba.escuchador.mdb.transformador.MRM;
import com.ba.escuchador.mdb.transformador.MSGaMRM;
import com.ba.escuchador.mdb.util.SalidaLogger;

public class PruebasPPROV extends SuitePruebas {
	private static int counter = 1;
	
	public static int pruebas() {
		SalidaLogger.logLnX("|---> Incian pruebas unitarias de PPROV");
		int result =  transformarPROV(counter++);
		SalidaLogger.logLnX("Terminan las pruebas unitarias de PPROV <---|");
		return result;
	}
	
	private static int transformarPROV(int counter){
		String name = new Throwable().getStackTrace()[0].getMethodName();
		Pattern patronCuerpo = Pattern.compile("([0-9]{12}[CA][0-9]{141}$)|([0-9]{12}[CA][0-9]{121}$)");
		Pattern patronEncabezado = Pattern.compile("[0-9]{12}PPROVO[0-9A-Fa-f]{9}");
		
		MRM expected = MSGaMRM.transform(provMsg);
		StringBuilder cuerpo = expected.getCuerpo();
		
		StringTokenizer line = new StringTokenizer(cuerpo.toString(), "\n");
		if(cuerpo.length()>0 && expected.getTipo().equals("PPROV") ){
			boolean correctFormat = true;
			String header = line.nextToken();
			Matcher matchEncabezado = patronEncabezado.matcher(header);
			if(!matchEncabezado.find()){
				correctFormat = false;
			}
			while(line.hasMoreTokens() && correctFormat){
				String body = line.nextToken();
				Matcher matchCuerpo = patronCuerpo.matcher(body);
				if(!matchCuerpo.find()){
					break;
				}
			}
			if(correctFormat)
				return resultadoExitoso(counter, name);
		}
		return resultadoFallido(counter, name);
	}

}

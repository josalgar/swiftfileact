package com.ba.escuchador.mdb.pruebas;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.ba.escuchador.mdb.propiedades.Constantes;
import com.ba.escuchador.mdb.propiedades.Propiedades;
import com.ba.escuchador.mdb.transformador.MRM;
import com.ba.escuchador.mdb.transformador.MSGaMRM;
import com.ba.escuchador.mdb.util.SalidaLogger;

public class PruebasXSL extends SuitePruebas{
	private static int counter = 1;
	
	public static int pruebas() {
		SalidaLogger.logLnX("|---> Incian pruebas unitarias de XSL");
		int result =  accesoXSL(counter++)+falloNumeroTransacciones(counter++);
		SalidaLogger.logLnX("Terminan las pruebas unitarias de XSL <---|");
		return result;
	}
	
	private static int accesoXSL(int counter){
		String name = new Throwable().getStackTrace()[0].getMethodName();
		Path xsl = Paths.get(Propiedades.obtenerPropiedadPorNombre(Constantes.PATH_XSL).trim());
		boolean exists = Files.exists(xsl);
		boolean readable = Files.isReadable(xsl);
		
		if(exists && readable){
			return resultadoExitoso(counter, name);
		}
		return resultadoFallido(counter, name);
		
	}
	
	private static int falloNumeroTransacciones(int counter){
		String name = new Throwable().getStackTrace()[0].getMethodName();
		MRM expected = MSGaMRM.transform(badMsg1);
		
		if(expected==null){
			return resultadoExitoso(counter, name);
		}
		return resultadoFallido(counter, name);
	}
	
	
}

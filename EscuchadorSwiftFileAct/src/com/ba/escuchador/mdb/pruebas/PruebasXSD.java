package com.ba.escuchador.mdb.pruebas;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;



import com.ba.escuchador.mdb.propiedades.Constantes;
import com.ba.escuchador.mdb.propiedades.Propiedades;
import com.ba.escuchador.mdb.util.SalidaLogger;
import com.ba.escuchador.mdb.validador.XSDActions;

public class PruebasXSD extends SuitePruebas{
	private static int counter = 1;
	
	public static int pruebas() {
		
		SalidaLogger.logLn("|---> Incian pruebas unitarias de XSD");
		int result =  accesoXSD(counter++)+ verificarMsgInvalido(counter++) + verificarMsgBienFormado(counter++);
		SalidaLogger.logLn("Terminan pruebas unitarias de XSD <---|");

		return result;
	}
	
	private static int accesoXSD(int counter){
		String name = new Throwable().getStackTrace()[0].getMethodName();
		Path xsd = Paths.get(Propiedades.obtenerPropiedadPorNombre(Constantes.PATH_XSD).trim());
		boolean exists = Files.exists(xsd);
		boolean readable = Files.isReadable(xsd);
		
		if(exists && readable){
			return resultadoExitoso(counter, name);
		}
		return resultadoFallido(counter, name);
	}

	private static int verificarMsgInvalido(int counter){
		String name = new Throwable().getStackTrace()[0].getMethodName();
		String msg = "INVALIDO";
		boolean resultado = XSDActions.validate(msg);
		if(!resultado){
			return resultadoExitoso(counter, name);
		}
		return resultadoFallido(counter, name);
		
	}

	private static int verificarMsgBienFormado(int counter){
		String name = new Throwable().getStackTrace()[0].getMethodName();
		boolean resultado = XSDActions.validate(achMsg);
		if(resultado){
			return resultadoExitoso(counter, name);
		}
		return resultadoFallido(counter, name);
	}
}

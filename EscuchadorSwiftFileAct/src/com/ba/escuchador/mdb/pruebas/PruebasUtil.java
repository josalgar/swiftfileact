package com.ba.escuchador.mdb.pruebas;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import com.ba.escuchador.mdb.transformador.GeneradorID;
import com.ba.escuchador.mdb.transformador.GeneradorNombre;
import com.ba.escuchador.mdb.util.SalidaLogger;

public class PruebasUtil extends SuitePruebas{
private static int counter = 1;
	
	public static int pruebas() {
		SalidaLogger.logLnX("|---> Incian pruebas unitarias de Util");
		int result =  crearNombre(counter++)+crearId(counter++);
		SalidaLogger.logLnX("Terminan las pruebas unitarias de Util <---|");
		return result;
	}
	
	private static int crearNombre(int counter){
		String name = new Throwable().getStackTrace()[0].getMethodName();
		String id = GeneradorNombre.formularNombreMRM();
		if(!id.isEmpty()){
			return resultadoExitoso(counter, name);
		}
		return resultadoFallido(counter, name);
	}
	private static int crearId(int counter){
		String name = new Throwable().getStackTrace()[0].getMethodName();
		try{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(new InputSource(new StringReader(achMsg)));
		String id = GeneradorID.extraerDeXML(document);
		if(!id.isEmpty()){
			return resultadoExitoso(counter, name);
		}
		return resultadoFallido(counter, name);
		}
		catch (Exception e) {
			return resultadoFallido(counter, name);
		}
	}
}

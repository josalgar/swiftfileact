package com.ba.escuchador.mdb.pruebas;

import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ba.escuchador.mdb.transformador.MRM;
import com.ba.escuchador.mdb.transformador.MSGaMRM;
import com.ba.escuchador.mdb.util.SalidaLogger;

public class PruebasPPACH extends SuitePruebas {
	private static int counter = 1;
	
	public static int pruebas() {
		SalidaLogger.logLnX("|---> Incian pruebas unitarias de PPACH");
		int result =  transformarACH(counter++);
		SalidaLogger.logLnX("Terminan las pruebas unitarias de PPACH <---|");
		return result;
	}
	
	private static int transformarACH(int counter){
		String name = new Throwable().getStackTrace()[0].getMethodName();
		Pattern patronCuerpo = Pattern.compile("([0-9]{12}[C][0-9]{121}[P][ ]{69}[M ][ ]$)|(^[0-9]{12}[A][0-9]{121}[A][ATPC][ ]{2}[0-9]{8}[A-Za-z0-9 -]{22}[0-9 ]{35}[T ][M ][ ])");
		Pattern patronEncabezado = Pattern.compile("[0-9]{12}PPACHA[0-9A-Fa-f]{9}");
		
		MRM expected = MSGaMRM.transform(achMsg);
		StringBuilder cuerpo = expected.getCuerpo();
		
		StringTokenizer line = new StringTokenizer(cuerpo.toString(), "\n");
		if(cuerpo.length()>0 && expected.getTipo().equals("PPACH") ){
			boolean correctFormat = true;
			String header = line.nextToken();
			Matcher matchEncabezado = patronEncabezado.matcher(header);
			if(!matchEncabezado.find()){
				correctFormat = false;
			}
			while(line.hasMoreTokens() && correctFormat){
				String body = line.nextToken();
				Matcher matchCuerpo = patronCuerpo.matcher(body);
				if(!matchCuerpo.find()){
					break;
				}
			}
			if(correctFormat)
				return resultadoExitoso(counter, name);
		}
		return resultadoFallido(counter, name);
	}
	
}

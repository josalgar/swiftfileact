package com.ba.escuchador.mdb.transformador;

import java.util.Calendar;
import java.util.TimeZone;

public class GeneradorNombre {
	public static String formularNombreMRM(){
	StringBuilder uniqueId = new StringBuilder();
	Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("America/El_Salvador"));
	uniqueId.setLength(0);
	
	
	int aux = cal.get(Calendar.YEAR)%2000;
	uniqueId.append("00").append(Integer.toHexString(aux));
	String nameSec1 = uniqueId.toString().substring(Integer.toHexString(aux).length(), uniqueId.toString().length());
	uniqueId.setLength(0);
	
	aux = cal.get(Calendar.MONTH);
	uniqueId.append("0").append(Integer.toHexString(aux));
	String nameSec2 = uniqueId.toString().substring(Integer.toHexString(aux).length(), uniqueId.toString().length());
	uniqueId.setLength(0);
	
	
	aux = cal.get(Calendar.DAY_OF_MONTH)*100 +  cal.get(Calendar.HOUR);
	uniqueId.append("000").append(Integer.toHexString(aux));
	String nameSec3 = uniqueId.toString().substring(Integer.toHexString(aux).length(), uniqueId.toString().length());
	uniqueId.setLength(0);
	
	
	aux = (cal.get(Calendar.MILLISECOND) + cal.get(Calendar.MINUTE)  + cal.get(Calendar.SECOND))%999;
	uniqueId.append("000").append(aux);
	String nameSec4 = uniqueId.toString().substring(Integer.toString(aux).length(), uniqueId.toString().length());
	uniqueId.setLength(0);
	
	uniqueId.append(nameSec1)
	        .append(nameSec2)
	        .append(nameSec3)
	        .append(nameSec4);
        return uniqueId.toString().toUpperCase();
	}
}

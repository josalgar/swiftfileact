package com.ba.escuchador.mdb.transformador;

import java.io.File;


import org.w3c.dom.Document;


import java.io.StringReader;
import java.io.StringWriter;

import java.text.Format;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;

import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.xml.sax.InputSource;

import com.ba.escuchador.mdb.propiedades.Constantes;
import com.ba.escuchador.mdb.propiedades.Propiedades;
import com.ba.escuchador.mdb.util.ControladorErrores;
import com.ba.escuchador.mdb.util.SalidaLogger;

/**
 *
 * @author AlexBig
 */
public class MSGaMRM {
	private static Document document;

	public static MRM transform(String msg) {
		try {
			
			Format dateFormat = new SimpleDateFormat("MMddyy");
			Format timeFormat = new SimpleDateFormat("HHmmss");
			Date currentDate = new Date();
			String filename = GeneradorNombre.formularNombreMRM();

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

			File xsl = new File(Propiedades.obtenerPropiedadPorNombre(Constantes.PATH_XSL).trim());

			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);

			DocumentBuilder builder = factory.newDocumentBuilder();
			document = builder.parse(new InputSource(new StringReader(msg)));

			if (xsl.exists()) {
				// Transformador
				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Templates xslTemplate = transformerFactory.newTemplates(new StreamSource(xsl));
				Transformer transformer = xslTemplate.newTransformer();

				// Inyeccion de parametros
				transformer.setParameter("filename", filename);
				transformer.setParameter("date", dateFormat.format(currentDate));
				transformer.setParameter("time", timeFormat.format(currentDate));

				DOMSource source = new DOMSource(document);
				// Transformar
				transformer.transform(source, result);
				StringBuffer sb = writer.getBuffer();

				String trama = sb.toString();
				
				ControladorErrores.verificarTrama(trama);
				

				return new MRM(trama, filename, document);
			} else {
				SalidaLogger.logErr("Archivo XSLT no encontrado");
			}
		} catch (Exception e) {
			SalidaLogger.logErr("Error al generar documento MRM <<<");
			SalidaLogger.logErrorStackTrace(e);
		}
		return null;
	}
}
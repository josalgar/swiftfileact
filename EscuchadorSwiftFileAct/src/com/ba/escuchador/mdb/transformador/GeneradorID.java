package com.ba.escuchador.mdb.transformador;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class GeneradorID {
	public static String extraerDeXML(Document document){
	 Element one = (Element)((Element)document.getElementsByTagName("Document").item(0))
             .getElementsByTagName("pain.001.001.02").item(0);
     Element two = (Element) one.getElementsByTagName("GrpHdr").item(0);
     return two.getElementsByTagName("MsgId").item(0).getTextContent();
	}
}

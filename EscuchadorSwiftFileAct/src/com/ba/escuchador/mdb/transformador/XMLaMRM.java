package com.ba.escuchador.mdb.transformador;

import java.io.File;
import java.io.StringWriter;
import java.nio.file.Path;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.w3c.dom.Document;

/**
 *
 * @author AlexBig
 */
public final class XMLaMRM {
    private static Document document;
    
    public static String transform(Path xmlPath, String xslFile){
        try {
            Format dateFormat = new SimpleDateFormat("ddMMyy");
            Format timeFormat = new SimpleDateFormat("HHmmss");
            Date currentDate = new Date();
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            File xml = xmlPath.toFile();
            File xsl = new File(xslFile);
            
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);

            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.parse(xml);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();

            StreamSource style = new StreamSource(xsl);
            Transformer transformer = transformerFactory.newTransformer(style);
            String fileName = xmlPath.getFileName().toString();
            if(fileName.contains(".")){
                fileName =fileName.substring(0, fileName.lastIndexOf("."));
            }
            transformer.setParameter("filename", fileName);
            transformer.setParameter("date", dateFormat.format(currentDate));
            transformer.setParameter("time", timeFormat.format(currentDate));
            
            DOMSource source = new DOMSource(document);

        
            transformer.transform(source, result);
            
            StringBuffer sb = writer.getBuffer(); 
            return sb.toString();
            }
        catch(Exception e){
           e.printStackTrace();
        }
        return "";
    }
}
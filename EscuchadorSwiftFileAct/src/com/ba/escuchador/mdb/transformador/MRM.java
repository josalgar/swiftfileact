package com.ba.escuchador.mdb.transformador;

import org.w3c.dom.Document;

public class MRM {
	private String nombre = "";
	private String nombreArchivo = "";
	private StringBuilder cuerpo = new StringBuilder();
	private String tipo = "";
	private String msgId = "";
	
	
	public MRM(String nombre, String cuerpo, String tipo, String msgId) {
		super();
		this.nombre = nombre;
		nombreArchivo = this.nombre;
		this.cuerpo.append(cuerpo);
		this.tipo = tipo;
		this.msgId = msgId;
	}
	
	
	public MRM(String trama, String nombre, Document document){
		super();
		this.nombre = trama.substring(17, 18) + nombre;
		nombreArchivo = this.nombre;
		this.cuerpo.append(trama);
		this.tipo = trama.substring(12, 17);
		this.msgId = GeneradorID.extraerDeXML(document);
	}
	
	public MRM(String trama){
		super();
		this.nombre = trama.substring(17, 27);
		nombreArchivo = this.nombre;
		this.cuerpo.append(trama);
		this.tipo = trama.substring(12, 17);
		this.msgId = trama.substring(0, 12);
	}
	


	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public StringBuilder getCuerpo() {
		return cuerpo;
	}
	public void setCuerpo(StringBuilder cuerpo) {
		this.cuerpo = cuerpo;
	}
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getMsgId() {
		return msgId;
	}


	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	
	public String getLetra(){
		return cuerpo.toString().substring(17, 18);
	}


	public String getNombreArchivo() {
		return nombreArchivo;
	}


	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	

	
	
}

package com.ba.escuchador.mdb.propiedades;

import org.apache.log4j.Logger;

/**
 *  Constantes utilizadas para obtener informacion del properties.
 *  Son los valores de las llaves del properties.
 **/
public class Constantes {
	///home/alfonso/PLANILLAS
	public static final String PATH_CREDENCIALES	 = "/home/ebanca/sv/SwiftFileAct/conf/config.properties";
	public static final String PATH_LOG4J			 = "/home/ebanca/sv/SwiftFileAct/conf/log4j.properties";
	public static final String LOGGER_NAME			 = "SwiftFileAct";
	public static final Logger infoLogger 					 = Logger.getLogger("infoLogger");
	public static final Logger errorLogger 				 = Logger.getLogger("errorLogger");
	
	public static final String PATH_XSD 			 = "PATH_XSD";
	public static final String PATH_MQ 				 = "PATH_MQ";
	public static final String PATH_XSL				 = "PATH_XSL";
	public static final String PATH_LOCAL 			 = "PATH_LOCAL";
	
	/** Servidor FTP **/
	public static final String SERVIDOR_ESCRIBE      = "SERVIDOR_ESCRIBE";
	public static final String USUARIO_ESCRIBE		 = "USUARIO_ESCRIBE";
	public static final String CLAVE_ESCRIBE       	 = "CLAVE_ESCRIBE";
	public static final String PUERTO_ESCRIBE        = "PUERTO_ESCRIBE";
	public static final String DIRECTORIO_ESCRIBE    = "DIRECTORIO_ESCRIBE";
	public static final String DIRECTORIO_RESPUESTA  = "DIRECTORIO_RESPUESTA";
	public static final String COLA_RESPUESTA 		 = "COLA_RESPUESTA";
	public static final String CONECTOR_PRINCIPAL 	 = "CONECTOR_PRINCIPAL";
	public static final String PATH_CONECTORES_CONFIG = "PATH_CONECTORES_CONFIG";
	public static final String USUARIO_CORE 		= "USUARIO_CORE";
	public static final String CLAVE_CORE 			= "CLAVE_CORE";
	public static final String LECTURA_CORE 		="LECTURA_CORE";
	public static final String RESPUESTA_CORE 		= "RESPUESTA_CORE"; 
//	LLAVE_PUBLICA=./.ssh/id_rsa
//			KNOW_HOSTS=./.ssh/know_hosts
	public static final String LLAVE_PRIVADA 		= "LLAVE_PRIVADA";
	public static final String KNOWN_HOSTS			= "KNOWN_HOSTS";
	
	public static final String ID_SERVICIO = "ID_SERVICIO";

	
	public static final String MINUTO_INICIO = "MINUTO_INICIO";
	public static final String PERIODO_EJECUCION	 = "PERIODO_EJECUCION";
	public static final String RUTA_TMP = "RUTA_TMP";
	
	
}


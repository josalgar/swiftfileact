package com.ba.escuchador.mdb.propiedades;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import com.ba.escuchador.mdb.util.SalidaLogger;

public class PropiedadesMQ {
	private final static String[] constantes = {"HOST", "MANAGER", "PUERTO", "CANAL", "COLA"};
	public static Map<String, String> mapa = new HashMap<String,String>();
	
	public static boolean obtenerPropiedades(StringTokenizer tokens){
		int i = 0;
		while(tokens.hasMoreTokens()){
			String token = tokens.nextToken();
			mapa.put(constantes[i], token);
			i++;
			if(i>constantes.length){
				SalidaLogger.logErr("Las propiedades de MQ no estan configuradas apropiedamente");
				return false;
			}
		}
		return true;
		
	}
	
	public static void inicializarPropieadesMQ(){
		InputStream inputStream = null;
		try{
			inputStream = new FileInputStream(Propiedades.obtenerPropiedadPorNombre(Constantes.PATH_CONECTORES_CONFIG));
			Properties properties = new Properties();
			properties.load(inputStream);
			String s_webServiceDbConfig = properties.getProperty(Propiedades.obtenerPropiedadPorNombre(Constantes.CONECTOR_PRINCIPAL));
			StringTokenizer tokens = new StringTokenizer(s_webServiceDbConfig, "|");
			obtenerPropiedades(tokens);
			
		}catch(Exception e){
			SalidaLogger.logErrorStackTrace(e);
			return;
		}finally{
			try{
				inputStream.close();
			}catch(Exception e1){
				
			}
		}
	}
	
	public static String obtenerPropiedadPorPosicion(int posicion){
		return mapa.get(constantes[posicion]);
	}
	
}

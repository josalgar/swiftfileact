package com.ba.escuchador.mdb.propiedades;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;


import com.ba.escuchador.mdb.util.SalidaLogger;

public class Propiedades {

	public static Map<String, String> mapa = new HashMap<String,String>();
	

	
	public static void iniciarPropiedades() {
		try {
			File f = new File(Constantes.PATH_CREDENCIALES);
			Properties prop = new Properties();
			prop.load(new FileInputStream(f));
			
			if (!prop.isEmpty()) {
				prop.forEach( (k,v) -> 
				mapa.put((String)k, (String)v));
			}
		} catch (Exception e) {
			SalidaLogger.logErrorStackTrace(e);
		}
	}
	
	public static String obtenerPropiedadPorNombre(String constantName){
		return mapa.get(constantName);
	}
	
	public static java.util.Date getFechaInicioEjecucion() {
		Calendar now = Calendar.getInstance();
		int minutoInicio = Integer.valueOf(mapa.get(Constantes.MINUTO_INICIO).toString());
		if (minutoInicio > 0) {
			now.add(Calendar.MINUTE, minutoInicio);
		} else {
			now.add(Calendar.SECOND, 10);
		}
		SalidaLogger.logLn("Fecha Inicio Ejecucion: "+now.getTime());
		return now.getTime();
	}
}

package com.ba.escuchador.mdb.operatividad;

import java.io.IOException;

import com.ba.escuchador.mdb.propiedades.Constantes;
import com.ba.escuchador.mdb.propiedades.Propiedades;
import com.ba.escuchador.mdb.util.SalidaLogger;
import com.ba.utilidades.sftp.SFTPClient;

public abstract class ConfiguracionSFTP {
	protected static String host = 			"";
	protected static String user =  		"";
	protected static String pass = 			"";
	protected static String pk = 			"";
	protected static String kh = 			"";
	protected static String ruta = 			"";
	protected static String passphrase = 	"";
	protected static int tmout = 			0;
	protected static boolean usepass = 		true;
	
	public static void establecerPropiedades() {
		host = Propiedades.mapa.get(Constantes.SERVIDOR_ESCRIBE);
		user = Propiedades.mapa.get(Constantes.USUARIO_ESCRIBE);
		pass = Propiedades.mapa.get(Constantes.CLAVE_ESCRIBE);
		pk = Propiedades.mapa.get(Constantes.LLAVE_PRIVADA);
		kh = Propiedades.mapa.get(Constantes.KNOWN_HOSTS);
		ruta = Propiedades.mapa.get(Constantes.DIRECTORIO_ESCRIBE);
		
	}
	
	public static SFTPClient crearSftp(){
		try {
			SFTPClient sftp = new SFTPClient(host, user, pass, passphrase, pk, kh, tmout, usepass);
			return sftp;
		} catch (IOException e) {
			SalidaLogger.logErrorStackTrace(e);
		}
		return null;
	}
	
	
}

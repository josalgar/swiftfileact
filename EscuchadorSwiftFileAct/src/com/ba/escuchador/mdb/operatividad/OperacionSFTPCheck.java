package com.ba.escuchador.mdb.operatividad;

import com.ba.escuchador.mdb.transformador.MRM;
import com.ba.escuchador.mdb.util.SalidaLogger;
import com.ba.utilidades.sftp.SFTPClient;
import com.jcraft.jsch.SftpException;

public class OperacionSFTPCheck extends ConfiguracionSFTP {
	public static boolean start(MRM mrm) {
		boolean resultado = false;

		SalidaLogger.logErr("-------Verificando si existe el archivo  "+ mrm.getNombre() +" en el SFTP");

		SalidaLogger.logLnX("-------Verificando si existe el archivo  "+ mrm.getNombre() +" en el SFTP");

		try {


			String nombreArchivoPeticion = mrm.getNombre();
			SFTPClient sftp = crearSftp();
			sftp.connect();

			SalidaLogger.logLnX("Conectado a  SFTP");

			
			resultado = !sftp.checkExists(ruta+nombreArchivoPeticion);
			
			if(resultado){
				SalidaLogger.logLnX("-------No existe el archivo "+ mrm.getNombre() +" insercion posible!");
			}
			
			sftp.disconnect();
			
			return resultado;


		} catch (SftpException e) {

			SalidaLogger.logErr("Error en transferencia");
			SalidaLogger.logErrorStackTrace(e);

		} catch (Exception e) {

			SalidaLogger.logErrorStackTrace(e);
		}


		return resultado;
	}
}

package com.ba.escuchador.mdb.operatividad;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import com.ba.escuchador.mdb.mq.CuerpoMensaje;
import com.ba.escuchador.mdb.mq.Notificacion;
import com.ba.escuchador.mdb.propiedades.Constantes;
import com.ba.escuchador.mdb.propiedades.Propiedades;
import com.ba.escuchador.mdb.transformador.GeneradorNombre;
import com.ba.escuchador.mdb.transformador.MRM;
import com.ba.escuchador.mdb.transformador.MSGaMRM;
import com.ba.escuchador.mdb.util.SalidaLogger;
import com.ba.escuchador.mdb.validador.XSDActions;

public class OperacionCentral {

	public void procesarMensaje(String msg) {
		SalidaLogger.logLnX("Procesando mensaje de entrada...");

		boolean aceptable = XSDActions.validate(msg);
		if (!aceptable) {
			SalidaLogger.logLnX(">> Error en formato de entrada");
		} else {
			SalidaLogger.logLnX("Formato valido! Transformando mensaje a MRM...");
			MRM mrm = MSGaMRM.transform(msg);
			if (mrm != null && !mrm.getTipo().equals("ERROR")) {
				procesarTrama(mrm);
			} else {
				SalidaLogger.logLnX(">> Trama erronea");
			}
		}
	}

	/**
	 */
	public void procesarTrama(MRM mrm) {
		
		boolean archivoTemporal = crearArchivoTmpLocal(mrm);
		if (!archivoTemporal)
			return;
		boolean insertable = verificarInsercion(mrm);
		if (!insertable)
			return;
		boolean transferido = transferirMensaje(mrm);
		if (!transferido)
			return;
		boolean notificado = notificarMensaje(mrm);
		if (!notificado)
			return;
		eliminaArchivoTemp(mrm);

	}
	
	
	private boolean crearArchivoTmpLocal(MRM mrm){
		
		String ruta = Propiedades.mapa.get(Constantes.RUTA_TMP);
		String mensaje = mrm.getCuerpo().toString();
		
		try {
			SalidaLogger.logLnX("Iniciando la Creacion...");
			File file = new File(ruta+"/"+mrm.getNombre());
			if(!file.exists()){
				file.createNewFile();
				FileWriter w = new FileWriter(file);
				BufferedWriter bw  = new BufferedWriter(w);
				bw.write(mensaje);
				bw.close();
				SalidaLogger.logLnX("Archivo Creado Con �xito!");
			}
			return true;
			
		} catch (Exception e) {
			SalidaLogger.logErrorStackTrace(e);
		}
		SalidaLogger.logLnX("Error en la Creacion");
		return false;
	}
	
	
	private boolean verificarInsercion(MRM mrm){
		SalidaLogger.logLnX(">> Verificando si existe un archivo con ese nombre en el SFTP <<");
		for(int i= 1; i<4; i++){
			SalidaLogger.logLnX("intento " + i + " de verificacion de insercion de archivo #:  " + mrm.getNombre() + ") ");
			boolean result = OperacionSFTPCheck.start(mrm);
			if(result){
				return true;
			}
			mrm.setNombre(mrm.getLetra()+GeneradorNombre.formularNombreMRM());
		}
		SalidaLogger.logLnX("Conflicto en el nombre de archivo");
		return false;
	}
	
	private boolean transferirMensaje(MRM mrm){
		SalidaLogger.logLnX(">> Transfiriendo mensaje al SFTP <<");
		for (int i = 1; i < 4; i++) {
			SalidaLogger.logLnX("intento " + i + " de envio de archivo descifrado al sftp #:  " + mrm.getNombre() + ") ");
			boolean result = OperacionSFTPUpload.start(mrm);
			if(result){
				return true;
			}
			
		}
		SalidaLogger.logLnX(
				"No se pudo realizar envio de archivo descifrado al sftp #:  " + mrm.getNombre() + ") ");
		return false;
	}
	
	private boolean notificarMensaje(MRM mrm){
		SalidaLogger.logLnX(">> Notificando al AS400 <<");
		for (int i = 1; i < 4; i++) {
			SalidaLogger.logLnX("intento " + i + " de envio de notificacion #:  " + mrm.getNombre() + ") ");
			boolean result = Notificacion.enviarRespuestaMQ(CuerpoMensaje.crearMensaje(mrm), null);
			if (result) {
				SalidaLogger.logLnX("Mensaje MQ exitosamente enviado");
				return true;
			}
		}

		SalidaLogger.logLnX("No se pudo notificar al AS400");
		return false;
	}
	
	private boolean eliminaArchivoTemp(MRM mrm){
		String ruta = Propiedades.mapa.get(Constantes.RUTA_TMP);
		try {
			SalidaLogger.logLnX("Iniciando Eliminacion...");
			File file = new File(ruta+"/"+mrm.getNombreArchivo());
			SalidaLogger.logLnX(ruta+"/"+mrm.getNombreArchivo());
			if(file.exists()){
				file.delete();
				SalidaLogger.logLnX("Archivo Exitosamente Eliminado");
				return true;
			}
			
		} catch (Exception e) {
			SalidaLogger.logErrorStackTrace(e);
			
		}
		SalidaLogger.logLnX("Error en la eliminacion");
		return false;
	}

}

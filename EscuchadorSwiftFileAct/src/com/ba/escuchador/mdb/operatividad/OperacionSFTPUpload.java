package com.ba.escuchador.mdb.operatividad;

import com.ba.escuchador.mdb.transformador.MRM;
import com.ba.escuchador.mdb.util.SalidaLogger;
import com.ba.utilidades.sftp.SFTPClient;
import com.jcraft.jsch.SftpException;

public class OperacionSFTPUpload extends ConfiguracionSFTP{
	public static boolean start(MRM mrm) {

		boolean resultado = false;

		SalidaLogger.logErr("-------Inicia transmision a SFTP");

		SalidaLogger.logLnX("-------Inicia transmision a SFTP");

		try {
				
				StringBuilder planillaASFTP = mrm.getCuerpo();
				String nombreArchivoPeticion = mrm.getNombre();
				SFTPClient sftp = crearSftp();
				sftp.connect();

				SalidaLogger.logLnX("Conectado a  SFTP");

				sftp.putFile(planillaASFTP, nombreArchivoPeticion, ruta);
				
				sftp.disconnect();

				resultado = true;

			} catch (SftpException e) {

				SalidaLogger.logErr("Error en transferencia");
				SalidaLogger.logErrorStackTrace(e);


			} catch (Exception e) {

				SalidaLogger.logErrorStackTrace(e);
			}


		return resultado;
	
	}
}
